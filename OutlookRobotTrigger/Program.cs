﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OutlookRobotTrigger
{
    static class Program
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        private static string LOGS_FILENAME;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LOGS_FILENAME = DateTime.Now.ToString("g").Replace('/', '-').Replace(':', '_') + ".txt";
            Logging.OnNewLog((s) => {
                if (!Directory.Exists("logs"))
                    Directory.CreateDirectory("logs");
                File.AppendAllText("logs\\" + LOGS_FILENAME, s + Environment.NewLine);
            });

            if (Environment.OSVersion.Version.Major >= 6)
                SetProcessDPIAware();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
