﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OL = Microsoft.Office.Interop.Outlook;

namespace OutlookRobotTrigger
{
    public class Outlook
    {
        private static Outlook _instance = null;
        public static Outlook GetInstance()
        {
            if (_instance == null)
                _instance = new Outlook();
            return _instance;
        }

        public OL.Application application { get; private set; }
        public OL.NameSpace nameSpace { get; private set; }

        private Outlook()
        {
            StartNew();
        }
        public void StartNew()
        {
            application = new OL.Application();
            nameSpace = application.GetNamespace("MAPI");
        }

        public string[] GetFolders()
        {
            List<string> folders = new List<string>();
            //if (nameSpace.Folders.Count >= 1)
            OL.MAPIFolder inboxFolder = nameSpace.GetDefaultFolder(OL.OlDefaultFolders.olFolderInbox);
            if (inboxFolder != null)
            {
                folders.Add(inboxFolder.FolderPath);
                GetFoldersNext(folders, inboxFolder);
            }
            return folders.ToArray();
        }
        void GetFoldersNext(List<string> folders, OL.MAPIFolder folder)
        {
            foreach(OL.MAPIFolder f in folder.Folders)
            {
                if (f.DefaultItemType != OL.OlItemType.olMailItem)
                    continue;
                folders.Add(f.FolderPath);
                GetFoldersNext(folders, f);
            }
        }

        public OL.MAPIFolder GetFolder(string folderName)
        {
            folderName = folderName.TrimStart('\\');
            string[] parts = folderName.Split('\\');
            OL.MAPIFolder folder = nameSpace.Folders[parts[0]];
            for (int i = 1; i < parts.Length; i++)
                folder = folder.Folders[parts[i]];
            return folder;
        }

    }

    public class OutlookException : Exception
    {
        public OutlookException(string message) : base(message) { }
    }
}
