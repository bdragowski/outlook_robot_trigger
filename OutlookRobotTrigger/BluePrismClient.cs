﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace OutlookRobotTrigger
{
    public class BluePrismClient
    {
        private HttpClient client;
        private string bpHost;

        public BluePrismClient(string host)
        {
            client = new HttpClient();
            bpHost = host;
            if (!bpHost.EndsWith("/"))
                bpHost += "/";
        }

        public void Authenticate(string username, string password)
        {
            if (CallCommand("user name " + username) != "USER SET")
                throw new BluePrismException("Cannot set user name.");
            if (CallCommand("password " + password) != "USER AUTHENTICATED")
                throw new BluePrismException("Cannot authenticate user password.");
        }

        public string GetUserId(string username)
        {
            string cmdResult = CallCommand("userlist");
            string[] users = cmdResult.Split('\n');
            foreach (string userline in users)
            {
                string ulTrimmed = userline.Trim();
                int delimiterPos = ulTrimmed.IndexOf(" - ");
                if (delimiterPos != 36)
                    throw new BluePrismException(cmdResult);

                string name = ulTrimmed.Substring(39);
                if (name == username)
                    return ulTrimmed.Substring(0, 36);
            }
            throw new BluePrismException("User GUID not found.");
        }
        public string GetProcId(string procname)
        {
            string cmdResult = CallCommand("proclist");
            string[] processes = cmdResult.Split('\n');
            foreach (string procline in processes)
            {
                string plTrimmed = procline.Trim();
                int delimiterPos = plTrimmed.IndexOf(" - ");
                if (delimiterPos != 36)
                    throw new BluePrismException(cmdResult);

                string name = plTrimmed.Substring(39);
                if (name == procname)
                    return plTrimmed.Substring(0, 36);
            }
            throw new BluePrismException("Prcess GUID not found.");
        }

        public string GetAuthToken(string procid, string userid, string password)
        {
            string token = CallCommand("getauthtoken " + procid + " " + userid + " " + password);
            if (token .Length != 73)
                throw new BluePrismException("Cannot create or invalid token: " + token);
            return token;
        }

        public string CreateSession(string token, string procid)
        {
            string sesResult = CallCommand("createas " + token + " " + procid);
            if (sesResult.Substring(0,18) != "SESSION CREATED : ")
                throw new BluePrismException("Cannot create or invalid session: " + sesResult);
            else
                return sesResult.Substring(18);
        }

        public void StartSession(string token, string sesid)
        {
            /*
            if ($startp != null)
        {
            $writer = new BPInputWriter();
            $input = $writer->writeInput($startp);
            $pResult = $this->callCommand("startp $input");
                if ($pResult != "PARAMETERS SET")
                throw new BluePrismException("Cannot set parameters: $pResult");
            }
            */

            string sResult = CallCommand("startas " + token + " " + sesid);
            if (sResult != "STARTED")
                throw new BluePrismException("Cannot start or invalid session: " + sResult);
        }


        public string CallCommand(string command)
        {
            try
            {
                var response = client.GetAsync(bpHost + command);
                response.Wait();
                var content = response.Result.Content;
                return content.ReadAsStringAsync().GetAwaiter().GetResult().Trim();
            }
            catch
            {
                throw new BluePrismException("Blue prism connection problem on " + bpHost);
            }
        }
    }

    public class BluePrismException : Exception
    {
        public BluePrismException(string message) : base(message) { }
    }
}
