﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookRobotTrigger
{
    public class BluePrismSetup
    {
        public class Process
        {
            public string name { get; private set; }
            public Process(string name)
            {
                if (String.IsNullOrEmpty(name))
                    throw new BluePrismException("Choose process name.");
                this.name = name;
            }
        }

        public string host { get; private set; }
        public string user { get; private set; }
        public string password { get; private set; }

        public Process process { get; private set; }

        public BluePrismSetup(string host, string user, string password)
        {
            this.host = host;
            this.user = user;
            this.password = password;
        }

        public void ConfigureProcess(Process process)
        {
            this.process = process;
        }

        public string[] GetProcessList()
        {
            BluePrismClient client = new BluePrismClient(host);
            client.Authenticate(user, password);
            List<string> processes = new List<string>();
            string proclist = client.CallCommand("proclist");
            foreach (string procline in proclist.Split('\n'))
            {
                string plTrimmed = procline.Trim();
                int delimiterPos = plTrimmed.IndexOf(" - ");
                if (delimiterPos != 36)
                    throw new BluePrismException(proclist);

                string name = plTrimmed.Substring(39);
                processes.Add(name);
            }
            return processes.ToArray();
        }

        private string cachedUserid = null;
        private string cachesProcid = null;
        public void CacheIDs()
        {
            BluePrismClient client = new BluePrismClient(host);
            client.Authenticate(user, password);
            cachedUserid = client.GetUserId(user);
            cachesProcid = client.GetProcId(process.name);
        }

        public void RunConfiguredProcess()
        {
            BluePrismClient client = new BluePrismClient(host);
            client.Authenticate(user, password);
            if (cachedUserid == null)
                cachedUserid = client.GetUserId(user);
            if (cachesProcid == null)
                cachesProcid = client.GetProcId(process.name);
            string token1 = client.GetAuthToken(cachesProcid, cachedUserid, password);
            string sesid = client.CreateSession(token1, cachesProcid);
            string token2 = client.GetAuthToken(cachesProcid, cachedUserid, password);
            client.StartSession(token2, sesid);
        }
    }
}
