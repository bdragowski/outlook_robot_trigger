﻿namespace OutlookRobotTrigger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnNewObserve = new System.Windows.Forms.Button();
            this.listObservedFolders = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabManagement = new System.Windows.Forms.TabPage();
            this.btnShowObserve = new System.Windows.Forms.Button();
            this.tabLogs = new System.Windows.Forms.TabPage();
            this.rtbLogs = new System.Windows.Forms.RichTextBox();
            this.timerOLWatchdog = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabManagement.SuspendLayout();
            this.tabLogs.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNewObserve
            // 
            this.btnNewObserve.AutoSize = true;
            this.btnNewObserve.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNewObserve.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNewObserve.Location = new System.Drawing.Point(3, 187);
            this.btnNewObserve.Name = "btnNewObserve";
            this.btnNewObserve.Size = new System.Drawing.Size(133, 28);
            this.btnNewObserve.TabIndex = 1;
            this.btnNewObserve.Text = "Watch new folder";
            this.btnNewObserve.UseVisualStyleBackColor = true;
            this.btnNewObserve.Click += new System.EventHandler(this.BtnNewObserve_Click);
            // 
            // listObservedFolders
            // 
            this.listObservedFolders.Dock = System.Windows.Forms.DockStyle.Top;
            this.listObservedFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listObservedFolders.FullRowSelect = true;
            this.listObservedFolders.Location = new System.Drawing.Point(3, 21);
            this.listObservedFolders.MultiSelect = false;
            this.listObservedFolders.Name = "listObservedFolders";
            this.listObservedFolders.ShowGroups = false;
            this.listObservedFolders.Size = new System.Drawing.Size(837, 160);
            this.listObservedFolders.TabIndex = 2;
            this.listObservedFolders.UseCompatibleStateImageBehavior = false;
            this.listObservedFolders.View = System.Windows.Forms.View.List;
            this.listObservedFolders.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ListObservedFolders_ItemSelectionChanged);
            this.listObservedFolders.SelectedIndexChanged += new System.EventHandler(this.ListObservedFolders_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Watched Outlook folders:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabManagement);
            this.tabControl1.Controls.Add(this.tabLogs);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(851, 395);
            this.tabControl1.TabIndex = 4;
            // 
            // tabManagement
            // 
            this.tabManagement.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabManagement.Controls.Add(this.btnShowObserve);
            this.tabManagement.Controls.Add(this.listObservedFolders);
            this.tabManagement.Controls.Add(this.label1);
            this.tabManagement.Controls.Add(this.btnNewObserve);
            this.tabManagement.Location = new System.Drawing.Point(4, 27);
            this.tabManagement.Name = "tabManagement";
            this.tabManagement.Padding = new System.Windows.Forms.Padding(3);
            this.tabManagement.Size = new System.Drawing.Size(843, 364);
            this.tabManagement.TabIndex = 0;
            this.tabManagement.Text = "Management";
            // 
            // btnShowObserve
            // 
            this.btnShowObserve.AutoSize = true;
            this.btnShowObserve.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowObserve.Enabled = false;
            this.btnShowObserve.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnShowObserve.Location = new System.Drawing.Point(142, 187);
            this.btnShowObserve.Name = "btnShowObserve";
            this.btnShowObserve.Size = new System.Drawing.Size(56, 28);
            this.btnShowObserve.TabIndex = 4;
            this.btnShowObserve.Text = "Show";
            this.btnShowObserve.UseVisualStyleBackColor = true;
            this.btnShowObserve.Click += new System.EventHandler(this.BtnShowObserve_Click);
            // 
            // tabLogs
            // 
            this.tabLogs.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabLogs.Controls.Add(this.rtbLogs);
            this.tabLogs.Location = new System.Drawing.Point(4, 27);
            this.tabLogs.Name = "tabLogs";
            this.tabLogs.Padding = new System.Windows.Forms.Padding(3);
            this.tabLogs.Size = new System.Drawing.Size(843, 364);
            this.tabLogs.TabIndex = 1;
            this.tabLogs.Text = "Logs";
            // 
            // rtbLogs
            // 
            this.rtbLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogs.Location = new System.Drawing.Point(3, 3);
            this.rtbLogs.Name = "rtbLogs";
            this.rtbLogs.ReadOnly = true;
            this.rtbLogs.Size = new System.Drawing.Size(837, 358);
            this.rtbLogs.TabIndex = 0;
            this.rtbLogs.Text = "";
            // 
            // timerOLWatchdog
            // 
            this.timerOLWatchdog.Enabled = true;
            this.timerOLWatchdog.Interval = 1000;
            this.timerOLWatchdog.Tick += new System.EventHandler(this.TimerOLWatchdog_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 395);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Outlook Robot Trigger";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabManagement.ResumeLayout(false);
            this.tabManagement.PerformLayout();
            this.tabLogs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNewObserve;
        private System.Windows.Forms.ListView listObservedFolders;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabManagement;
        private System.Windows.Forms.TabPage tabLogs;
        private System.Windows.Forms.RichTextBox rtbLogs;
        private System.Windows.Forms.Button btnShowObserve;
        private System.Windows.Forms.Timer timerOLWatchdog;
    }
}

