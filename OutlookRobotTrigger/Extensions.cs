﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OutlookRobotTrigger
{
    public static class Extensions
    {
        public static void WriteStringEx(this BinaryWriter writer, string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            for (int i = 0; i < bytes.Length; i++)
                bytes[i] = (byte)(255 - bytes[i]);
            writer.Write(bytes.Length);
            writer.Write(bytes);
        }
        public static string ReadStringEx(this BinaryReader reader)
        {
            int len = reader.ReadInt32();
            byte[] bytes = reader.ReadBytes(len);
            for (int i = 0; i < bytes.Length; i++)
                bytes[i] = (byte)(255 - bytes[i]);
            return Encoding.UTF8.GetString(bytes);
        }
    }
}
