﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookRobotTrigger
{
    public partial class ScanForm : Form
    {
        public Observation observation { get; private set; }


        public ScanForm()
        {
            InitializeComponent();
        }

        public DialogResult ShowNew(Form parentForm)
        {
            this.Text = "New Trigger";

            cbOutlookFolder.Enabled = true;
            btnRefreshFolders.Visible = true;
            BtnRefreshFolders_Click(null, null);

            foreach (Control c in panelRunBluePrism.Controls) c.Enabled = true;
            txtBluePrismPassword.UseSystemPasswordChar = true;

            txtBluePrismHost.Text = "";
            txtBluePrismUsername.Text = "";
            txtBluePrismPassword.Text = "";
            cbRunBluePrism.Enabled = true;
            cbRunBluePrism.Checked = false;
            observation = null;

            btnShowBPPassword.Visible = false;

            btnOk.Visible = false;
            btnDelete.Visible = false;
            btnAdd.Visible = true;
            btnCancel.Visible = true;

            return ShowDialog(parentForm);
        }

        public DialogResult ShowPreview(Form parentForm, Observation observation)
        {
            this.Text = "Trigger Preview";

            cbOutlookFolder.Items.Clear();
            cbOutlookFolder.Items.Add(observation.outlookFolder);
            cbOutlookFolder.Text = observation.outlookFolder;
            cbOutlookFolder.Enabled = false;
            btnRefreshFolders.Visible = false;

            txtBluePrismHost.Text = "";
            txtBluePrismUsername.Text = "";
            txtBluePrismPassword.Text = "";
            cbRunBluePrism.Enabled = false;
            if (observation.bpSetup != null)
            {
                cbRunBluePrism.Checked = true;
                txtBluePrismHost.Text = observation.bpSetup.host;
                txtBluePrismUsername.Text = observation.bpSetup.user;
                txtBluePrismPassword.Text = "******";
                cbBluePrismProcess.Items.Clear();
                cbBluePrismProcess.Items.Add(observation.bpSetup.process.name);
                cbBluePrismProcess.Text = observation.bpSetup.process.name;
            }
            else
            {
                cbRunBluePrism.Checked = false;
                txtBluePrismHost.Text = "";
                txtBluePrismUsername.Text = "";
                txtBluePrismPassword.Text = "";
                cbBluePrismProcess.Text = "";
            }
            panelRunBluePrism.Enabled = true;
            foreach (Control c in panelRunBluePrism.Controls) c.Enabled = false;
            txtBluePrismPassword.UseSystemPasswordChar = true;
            btnShowBPPassword.Enabled = true;

            btnShowBPPassword.Visible = true;

            btnOk.Visible = true;
            btnDelete.Visible = true;
            btnAdd.Visible = false;
            btnCancel.Visible = false;

            this.observation = observation;

            return ShowDialog(parentForm);
        }

        private void BtnRefreshFolders_Click(object sender, EventArgs e)
        {
            cbOutlookFolder.Items.Clear();
            cbOutlookFolder.Items.AddRange(Outlook.GetInstance().GetFolders());
        }

        private void CbRunBluePrism_CheckedChanged(object sender, EventArgs e)
        {
            panelRunBluePrism.Enabled = cbRunBluePrism.Checked;
        }

        private void BtnBluePrismCheck_Click(object sender, EventArgs e)
        {
            string host = txtBluePrismHost.Text;
            string user = txtBluePrismUsername.Text;
            string password = txtBluePrismPassword.Text;
            btnBluePrismCheck.Enabled = false;
            BluePrismSetup setup = new BluePrismSetup(host, user, password);
            try
            {
                string[] processes = setup.GetProcessList();
                cbBluePrismProcess.Items.AddRange(processes);
                cbBluePrismProcess.Enabled = true;
            }
            catch (BluePrismException ex)
            {
                MessageBox.Show(ex.Message, "BP Connection Problem", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            btnBluePrismCheck.Enabled = true;
        }

        private void TxtBluePrism_TextChanged(object sender, EventArgs e)
        {
            if (!((TextBox)sender).Enabled)
                return;
            cbBluePrismProcess.Text = "";
            cbBluePrismProcess.Items.Clear();
            cbBluePrismProcess.Enabled = false;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Observation obsrv;
            try
            {
                obsrv = Observation.New(cbOutlookFolder.Text);
                if (cbRunBluePrism.Checked)
                {
                    string host = txtBluePrismHost.Text;
                    string user = txtBluePrismUsername.Text;
                    string password = txtBluePrismPassword.Text;
                    BluePrismSetup setup = new BluePrismSetup(host, user, password);

                    string procname = cbBluePrismProcess.Text;
                    BluePrismSetup.Process proc = new BluePrismSetup.Process(procname);
                    setup.ConfigureProcess(proc);

                    obsrv.ConfigureBluePrismSetup(setup);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Invalid form", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            observation = obsrv;
            observation.Start();
            DialogResult = DialogResult.OK;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete observation on "+observation.outlookFolder+" folder?", "Delete Observation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                observation.DeleteSelf();
                observation = null;
                DialogResult = DialogResult.OK;
            }
        }

        private void BtnShowBPPassword_Click(object sender, EventArgs e)
        {
            if (observation != null && observation.bpSetup != null)
            {
                Logging.Log("Showed BP password for {0} (user: {1}, folder: {2})", observation.bpSetup.host, observation.bpSetup.user, observation.outlookFolder);
            }
            txtBluePrismPassword.UseSystemPasswordChar = false;
            txtBluePrismPassword.Text = observation.bpSetup.password;
            btnShowBPPassword.Enabled = false;
        }
    }
}
