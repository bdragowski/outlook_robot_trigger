﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookRobotTrigger
{
    public static class Logging
    {
        private static List<Action<string>> newLogHandlers = new List<Action<string>>();
        public static void OnNewLog(Action<string> handler)
        {
            newLogHandlers.Add(handler);
        }

        public static void Log(string message)
        {
            string log = String.Format("[{0}]   {1}", DateTime.Now, message);
            newLogHandlers.ForEach(h => h.Invoke(log));
        }
        public static void Log(string message, params object[] args)
        {
            message = String.Format(message, args);
            string log = String.Format("[{0}]   {1}", DateTime.Now, message);
            newLogHandlers.ForEach(h => h.Invoke(log));
        }
    }
}
