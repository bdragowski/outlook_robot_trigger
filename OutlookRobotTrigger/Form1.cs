﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookRobotTrigger
{
    public partial class Form1 : Form
    {

        Outlook outlook;
        ScanForm scanForm;

        public Form1()
        {
            InitializeComponent();
            outlook = Outlook.GetInstance();
            scanForm = new ScanForm();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Logging.OnNewLog((message) => {
                this.Invoke((Action)(() => { rtbLogs.Text += message + "\n"; }));
            });
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            foreach (Observation obsrv in Observation.Load())
                obsrv.Start();
            UpdateObservationLit();
        }

        private void BtnNewObserve_Click(object sender, EventArgs e)
        {
            if (scanForm.ShowNew(this) != DialogResult.Cancel)
            {
                Logging.Log("Added new trigger on {0} folder", scanForm.observation.outlookFolder);
                UpdateObservationLit();
                Observation.Save();
            }
        }

        private void UpdateObservationLit()
        {
            listObservedFolders.Items.Clear();
            foreach(Observation obsrv in Observation.GetObservations())
                listObservedFolders.Items.Add(obsrv.outlookFolder);
            btnShowObserve.Enabled = false;
        }

        private void ListObservedFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnShowObserve.Enabled = listObservedFolders.SelectedItems.Count > 0;
        }
        private void ListObservedFolders_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            btnShowObserve.Enabled = listObservedFolders.SelectedItems.Count > 0;
        }

        private void BtnShowObserve_Click(object sender, EventArgs e)
        {
            var selectedItem = listObservedFolders.SelectedItems[0];
            Observation obsrv = Observation.GetByFolder(selectedItem.Text);
            if (scanForm.ShowPreview(this, obsrv) != DialogResult.Cancel)
            {
                UpdateObservationLit();
                Observation.Save();
            }
        }

        private void TimerOLWatchdog_Tick(object sender, EventArgs e)
        {
            try
            {
                var something = outlook.nameSpace.CurrentUser;
            }
            catch
            {
                Logging.Log("Outlook connection lost!");
                Logging.Log("Reconnecting Outlook application...");
                outlook.StartNew();
                Logging.Log("Starting triggers...");
                foreach (Observation obsrv in Observation.GetObservations())
                    obsrv.Start();
                Logging.Log("Done");
            }
        }
    }
}
