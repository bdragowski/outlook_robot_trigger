﻿namespace OutlookRobotTrigger
{
    partial class ScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbOutlookFolder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRefreshFolders = new System.Windows.Forms.Button();
            this.panelRunBluePrism = new System.Windows.Forms.Panel();
            this.btnBluePrismCheck = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cbBluePrismProcess = new System.Windows.Forms.ComboBox();
            this.txtBluePrismPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBluePrismUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBluePrismHost = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbRunBluePrism = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnShowBPPassword = new System.Windows.Forms.Button();
            this.panelRunBluePrism.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbOutlookFolder
            // 
            this.cbOutlookFolder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutlookFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbOutlookFolder.FormattingEnabled = true;
            this.cbOutlookFolder.Location = new System.Drawing.Point(16, 34);
            this.cbOutlookFolder.Name = "cbOutlookFolder";
            this.cbOutlookFolder.Size = new System.Drawing.Size(518, 26);
            this.cbOutlookFolder.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Watch this Outlook folder:";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(465, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(61, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnRefreshFolders
            // 
            this.btnRefreshFolders.AutoSize = true;
            this.btnRefreshFolders.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRefreshFolders.Location = new System.Drawing.Point(540, 33);
            this.btnRefreshFolders.Name = "btnRefreshFolders";
            this.btnRefreshFolders.Size = new System.Drawing.Size(68, 27);
            this.btnRefreshFolders.TabIndex = 3;
            this.btnRefreshFolders.Text = "Refresh";
            this.btnRefreshFolders.UseVisualStyleBackColor = true;
            this.btnRefreshFolders.Click += new System.EventHandler(this.BtnRefreshFolders_Click);
            // 
            // panelRunBluePrism
            // 
            this.panelRunBluePrism.Controls.Add(this.btnShowBPPassword);
            this.panelRunBluePrism.Controls.Add(this.btnBluePrismCheck);
            this.panelRunBluePrism.Controls.Add(this.label5);
            this.panelRunBluePrism.Controls.Add(this.cbBluePrismProcess);
            this.panelRunBluePrism.Controls.Add(this.txtBluePrismPassword);
            this.panelRunBluePrism.Controls.Add(this.label4);
            this.panelRunBluePrism.Controls.Add(this.txtBluePrismUsername);
            this.panelRunBluePrism.Controls.Add(this.label3);
            this.panelRunBluePrism.Controls.Add(this.txtBluePrismHost);
            this.panelRunBluePrism.Controls.Add(this.label2);
            this.panelRunBluePrism.Enabled = false;
            this.panelRunBluePrism.Location = new System.Drawing.Point(12, 108);
            this.panelRunBluePrism.Name = "panelRunBluePrism";
            this.panelRunBluePrism.Padding = new System.Windows.Forms.Padding(3);
            this.panelRunBluePrism.Size = new System.Drawing.Size(596, 176);
            this.panelRunBluePrism.TabIndex = 4;
            // 
            // btnBluePrismCheck
            // 
            this.btnBluePrismCheck.AutoSize = true;
            this.btnBluePrismCheck.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnBluePrismCheck.Location = new System.Drawing.Point(131, 111);
            this.btnBluePrismCheck.Name = "btnBluePrismCheck";
            this.btnBluePrismCheck.Size = new System.Drawing.Size(257, 27);
            this.btnBluePrismCheck.TabIndex = 8;
            this.btnBluePrismCheck.Text = "Check connection and get process list";
            this.btnBluePrismCheck.UseVisualStyleBackColor = true;
            this.btnBluePrismCheck.Click += new System.EventHandler(this.BtnBluePrismCheck_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(1, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "Process";
            // 
            // cbBluePrismProcess
            // 
            this.cbBluePrismProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBluePrismProcess.Enabled = false;
            this.cbBluePrismProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbBluePrismProcess.FormattingEnabled = true;
            this.cbBluePrismProcess.Location = new System.Drawing.Point(131, 144);
            this.cbBluePrismProcess.Name = "cbBluePrismProcess";
            this.cbBluePrismProcess.Size = new System.Drawing.Size(391, 26);
            this.cbBluePrismProcess.TabIndex = 6;
            // 
            // txtBluePrismPassword
            // 
            this.txtBluePrismPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtBluePrismPassword.Location = new System.Drawing.Point(131, 66);
            this.txtBluePrismPassword.Name = "txtBluePrismPassword";
            this.txtBluePrismPassword.Size = new System.Drawing.Size(219, 24);
            this.txtBluePrismPassword.TabIndex = 5;
            this.txtBluePrismPassword.UseSystemPasswordChar = true;
            this.txtBluePrismPassword.TextChanged += new System.EventHandler(this.TxtBluePrism_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password:";
            // 
            // txtBluePrismUsername
            // 
            this.txtBluePrismUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtBluePrismUsername.Location = new System.Drawing.Point(131, 36);
            this.txtBluePrismUsername.Name = "txtBluePrismUsername";
            this.txtBluePrismUsername.Size = new System.Drawing.Size(219, 24);
            this.txtBluePrismUsername.TabIndex = 3;
            this.txtBluePrismUsername.TextChanged += new System.EventHandler(this.TxtBluePrism_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username:";
            // 
            // txtBluePrismHost
            // 
            this.txtBluePrismHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtBluePrismHost.Location = new System.Drawing.Point(131, 6);
            this.txtBluePrismHost.Name = "txtBluePrismHost";
            this.txtBluePrismHost.Size = new System.Drawing.Size(219, 24);
            this.txtBluePrismHost.TabIndex = 1;
            this.txtBluePrismHost.TextChanged += new System.EventHandler(this.TxtBluePrism_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Host:";
            // 
            // cbRunBluePrism
            // 
            this.cbRunBluePrism.AutoSize = true;
            this.cbRunBluePrism.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbRunBluePrism.Location = new System.Drawing.Point(16, 80);
            this.cbRunBluePrism.Name = "cbRunBluePrism";
            this.cbRunBluePrism.Size = new System.Drawing.Size(433, 22);
            this.cbRunBluePrism.TabIndex = 5;
            this.cbRunBluePrism.Text = "Run Blue Prism process when new email arrives to this folder";
            this.cbRunBluePrism.UseVisualStyleBackColor = true;
            this.cbRunBluePrism.CheckedChanged += new System.EventHandler(this.CbRunBluePrism_CheckedChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.AutoSize = true;
            this.btnAdd.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAdd.Location = new System.Drawing.Point(574, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(43, 27);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.btnOk);
            this.flowLayoutPanel1.Controls.Add(this.btnCancel);
            this.flowLayoutPanel1.Controls.Add(this.btnDelete);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 415);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(620, 35);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // btnOk
            // 
            this.btnOk.AutoSize = true;
            this.btnOk.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Location = new System.Drawing.Point(532, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(36, 27);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.AutoSize = true;
            this.btnDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDelete.Location = new System.Drawing.Point(400, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(59, 27);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnShowBPPassword
            // 
            this.btnShowBPPassword.AutoSize = true;
            this.btnShowBPPassword.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowBPPassword.Location = new System.Drawing.Point(356, 63);
            this.btnShowBPPassword.Name = "btnShowBPPassword";
            this.btnShowBPPassword.Size = new System.Drawing.Size(52, 27);
            this.btnShowBPPassword.TabIndex = 9;
            this.btnShowBPPassword.Text = "Show";
            this.btnShowBPPassword.UseVisualStyleBackColor = true;
            this.btnShowBPPassword.Click += new System.EventHandler(this.BtnShowBPPassword_Click);
            // 
            // ScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.cbRunBluePrism);
            this.Controls.Add(this.panelRunBluePrism);
            this.Controls.Add(this.btnRefreshFolders);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbOutlookFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "ScanForm";
            this.Text = "New Trigger";
            this.panelRunBluePrism.ResumeLayout(false);
            this.panelRunBluePrism.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbOutlookFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRefreshFolders;
        private System.Windows.Forms.Panel panelRunBluePrism;
        private System.Windows.Forms.CheckBox cbRunBluePrism;
        private System.Windows.Forms.Button btnBluePrismCheck;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbBluePrismProcess;
        private System.Windows.Forms.TextBox txtBluePrismPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBluePrismUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBluePrismHost;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnShowBPPassword;
    }
}