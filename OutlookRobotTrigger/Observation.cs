﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
using System.IO;


namespace OutlookRobotTrigger
{
    public class Observation
    {
        private const int FILEVERSION = 1;
        private const string FILENAME = "ORT_triggers.data";

        private static List<Observation> container = new List<Observation>();
        public static IEnumerable<Observation> GetObservations()
        {
            return container.AsReadOnly();
        }

        public string outlookFolder { get; private set; }
        public BluePrismSetup bpSetup { get; private set; }

        public static Observation New(string outlookFolder)
        {
            if (String.IsNullOrEmpty(outlookFolder))
                throw new OutlookException("Choose outlook folder.");

            if (container.Exists((o) => o.outlookFolder == outlookFolder))
                throw new OutlookException("This outlook folder is already under observation.");

            Observation obsrv = new Observation();
            obsrv.outlookFolder = outlookFolder;

            container.Add(obsrv);

            return obsrv;
        }
        public static Observation GetByFolder(string folder)
        {
            foreach (Observation obsrv in container)
                if (obsrv.outlookFolder == folder)
                    return obsrv;
            return null;
        }

        private bool _removed = false;
        private MAPIFolder _folder = null;
        private Observation()
        {}

        public void ConfigureBluePrismSetup(BluePrismSetup bpSetup)
        {
            this.bpSetup = bpSetup;
        }

        public void Start()
        {
            try
            {
                _folder = Outlook.GetInstance().GetFolder(outlookFolder);
            }
            catch
            {
                Logging.Log("Cannot start trigger! Not found folder: {0}", outlookFolder);
                return;
            }
            _folder.Items.ItemAdd += Items_ItemAdd;
            if (bpSetup != null)
            {
                try
                {
                    bpSetup.CacheIDs();
                }
                catch (BluePrismException ex)
                {}
            }
        }

        public void DeleteSelf()
        {
            Logging.Log("Trigger on {0} folder has been removed", outlookFolder);
            if (_folder != null)
                _folder.Items.ItemAdd -= Items_ItemAdd;
            container.Remove(this);
            _removed = true;
        }

        private void Items_ItemAdd(object Item)
        {
            if (_removed)
                return;
            var mail = (MailItem)Item;
            Logging.Log( "New email in {0}", outlookFolder );
            Task.Run(() => OnNewEmail(mail));
        }

        private void OnNewEmail(MailItem mail)
        {
            if (bpSetup != null)
            {
                Task.Run(() =>
                {
                    try
                    {
                        Logging.Log("[BP] Starting \"{0}\" on {1}...", bpSetup.process.name, bpSetup.host);
                        bpSetup.RunConfiguredProcess();
                        Logging.Log("[BP] New process started on {0}", bpSetup.host);
                    }
                    catch (BluePrismException ex)
                    {
                        Logging.Log("[BP] Cannot start process on {0}. Reason: {1}", bpSetup.host, ex.Message);
                    }
                });
            }
        }


        ///////////////////////////////
        public static void Save()
        {
            BinaryWriter writer= new BinaryWriter(File.Create(FILENAME));
            writer.Write(FILEVERSION);
            writer.Write(container.Count);
            foreach(Observation obsrv in container)
            {
                writer.WriteStringEx(obsrv.outlookFolder);
                writer.Write(obsrv.bpSetup != null);
                if (obsrv.bpSetup != null)
                {
                    writer.WriteStringEx(obsrv.bpSetup.host);
                    writer.WriteStringEx(obsrv.bpSetup.user);
                    writer.WriteStringEx(obsrv.bpSetup.password);
                    writer.WriteStringEx(obsrv.bpSetup.process.name);
                }
            }
            writer.Close();
            writer.Dispose();
        }
        public static Observation[] Load()
        {
            if (!File.Exists(FILENAME))
                return new Observation[0];
            BinaryReader reader = new BinaryReader(File.OpenRead(FILENAME));
            if (reader.ReadInt32() != FILEVERSION)
                return new Observation[0];
            List<Observation> added = new List<Observation>();
            int count = reader.ReadInt32();
            while (--count >= 0)
            {
                Observation obsrv = Observation.New(reader.ReadStringEx());
                if (reader.ReadBoolean())
                {
                    BluePrismSetup bps = new BluePrismSetup(
                            reader.ReadStringEx(), // host
                            reader.ReadStringEx(), // user
                            reader.ReadStringEx() // password
                        );
                    BluePrismSetup.Process proc = new BluePrismSetup.Process(reader.ReadStringEx()); // procname
                    bps.ConfigureProcess(proc);
                    obsrv.ConfigureBluePrismSetup(bps);
                }
                added.Add(obsrv);
            }
            reader.Close();
            reader.Dispose();
            return added.ToArray();
        }
    }
}
